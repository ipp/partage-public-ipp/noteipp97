# Note IPP n° 96 

La maquette RShiny et la maquette Excel accompagnent la publication de la note IPP n°96 : "Perte d'autonomie des personnes âgées : quels besoins et quels coûts pour accompagner le virage domiciliaire ?".
Lien vers la note :https://www.ipp.eu/publications/notes-ipp/

Source des données : maquette LIVIA (DREES), données DREES, CNSA, Insee ; traitements : Pauline Mendras, Institut des politiques publiques.
